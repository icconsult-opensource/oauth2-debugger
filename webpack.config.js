module.exports = {
  target: 'node',
  node: {
        fs: 'empty'
    },
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST, GET',
      'Access-Control-Allow-Headers': 'Access-Control-Allow-Methods, Access-Control-Allow-Origin, Origin, Accept, Content-Type',
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  }
};
