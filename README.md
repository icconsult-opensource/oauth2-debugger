# OAuth2Debugger

This is an webapp to debug the complete OAuth2 and OIDC process. It is written with VueJS, Bootstrap and NodeJS.

## Installation

1. Clone this repository to your system.
2. Open up a terminal and go to the directory of the project.
3. Run the command ```npm install```
4. Run the command ```npm run start```
5. Open the browser of your choice and go to localhost:8080/

## What you need

You need: 
- the links from the identity-provider:
	1. Authorize URI
	2. Access-Token URI
	3. Userinfo URI
- client ID
- client Secret
- redirect URI

The links can be found by searching with google.
You can get a client ID and client Secret by registering a client at the certain identity-provider.
Once you've done that, you can set a redirect URI (aka callback URI) at the certain page of the identity-provider.


## TODO

- Refresh-Token


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
