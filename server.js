const express = require('express')
const rp = require('request-promise')
const bodyparser = require('body-parser')
const cors = require('cors')

const app = express()

app.use(cors())

app.post('/', bodyparser.json(), (req, res) => {
  rp.post({
    url: req.body.accesstokenURI,
    body: 'grant_type=' + req.body.grant_type +
        '&client_id=' + req.body.client_id +
        '&redirect_uri=' + encodeURI(req.body.redirect_uri) +
        '&client_secret=' + req.body.client_secret +
        '&code=' + req.body.code +
        '&code_verifier=' + req.body.code_verifier,
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  .then((response) => {
    console.log(response);
    res.send(response)
  })
  .catch((error) => {
    console.log(error)
    res.status(500).send(error)
  })
})

app.post('/userinfo', bodyparser.json(), (req, res) => {
  rp.get({
    url: req.body.userinfoUrl,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${req.body.accessToken2}`
    }
  })
  .then((body) => {
    res.send(body)
  })
  .catch((error) => {
    console.log(error)
    res.status(500).send(error)
  })
})

app.listen(8000)
